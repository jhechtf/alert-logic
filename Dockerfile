FROM node:11.10-alpine as base
WORKDIR /var/app
ADD ./ /var/app/
RUN yarn && yarn build

FROM nginx:alpine as production
COPY --from=base /var/app/dist /usr/share/nginx/html
ADD nginx.production.conf /etc/nginx/conf.d/default.conf