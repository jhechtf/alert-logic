# Alert Logic 

## Local Setup

A local version of the website can be run and tested by going into the folder and running `yarn serve`. There should be a `pre` hook on the serve command which should install any node dependencies that are missing before it runs the development server. The server can take a minute to startup, but it will let you know when you are done. You can then view the website on `http://localhost:8090/`.

## Live Setup 

A production verison of the website can be viewed at https://alert.burbridge.dev/