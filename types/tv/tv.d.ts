
declare namespace Show {
    
    // Show creators
    export interface Creator{
        id: number;
        credit_id: string;
        name: string;
        gender: number;
        profile_path: string;
    }

    // Genres
    export interface Genre{
        id: number;
        name: string;
    }

    // Network
    export interface Network {
        name: string;
        id: number;
        logo_path: string;
        origin_country: string;
    }

    // Season
    export interface Season {
        air_date: string;
        episode_count: number;
        id: number;
        name: string;
        overview: string;
        poster_path: string;
        season_number: number;
    }

    // Production Company
    export interface ProductionCompany {
        id: number;
        logo_path ? : string;
        name: string;
        origin_country: string;
    }

    // Episode 
    export interface Episode {
        air_date: string;
        episode_number: number;
        id: number;
        name: string;
        overview: string;
        production_code: string;
        season_number: number;
        show_id: number;
        still_path: string;
        vote_average: number;
        vote_count: number;
    }

    // Show Details
    export interface Details {
        backdrop_path?: string;
        created_by?: Creator[];
        episode_run_time?: number[];
        first_air_date?: string;
        genres?: Genre[];
        homepage?: string;
        id?: number;
        in_production?: boolean;
        languages?: string[];
        last_air_date?: string;
        last_episode_to_air?: Episode;
        name?: string;
        networks?: Network[];
        number_of_episodes?: number;
        number_of_seasons?: number;
        origin_country?: string[];
        oroginal_language?: string;
        original_name?: string;
        overview?: string;
        popularity?: number;
        poster_path?: string;
        production_companies?: ProductionCompany[];
        seasons?: Season[];
        status?: string;
        type?: string;
        vote_average?: number;
        vote_count?: number;
    }

    export interface Cast{
        character: string;
        credit_id: string;
        gender?: number;
        id: number;
        name: string;
        order: number;
        profile_path?: string;
    }

    export interface Crew{
        credit_id: string;
        department: string;
        gender?: number;
        id: number;
        job: string;
        name: string;
        profile_path?: string;
    }
}