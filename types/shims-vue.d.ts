
import tmdb from '@lib/tmdb/tmdb';
declare module 'vue/types/vue' {
  export interface Vue {
    $tmdb: tmdb;
  }
}