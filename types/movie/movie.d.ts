declare namespace Movie {
    export enum Status {
        "Rumored",
        "Planned",
        "In Production",
        "Post Production",
        "Released",
        "Cancelled"
    }

    export interface Details{
        adult?: boolean;
        backdrop_path?: string;
        belongs_to_collection?: object;
        budget?: number;
        genres?: Show.Genre[];
        homepage?: string;
        id?: number;
        imdb_id?: string;
        original_language?: string;
        original_title?: string;
        overview?: string;
        popularity?: number;
        poster_path?: string;
        release_date?: string;
        production_company?: Show.ProductionCompany[];
        production_countries?: ProductionCountry[];
        revenue?: number;
        runtime?: number;
        spoken_languages?: SpokenLanguage[];
        status?: Status;
        tagline?: string;
        title?: string;
        video?: boolean;
        vote_average?: number;
        vote_count?: number;
    }
    export interface SpokenLanguage{
        iso_639_1: string;
        name: string;
    }
    export interface ProductionCountry{
        iso_3166_1: string;
        name: string;
    }
    export interface Cast{
        cast_id: number;
        character: string;
        credit_id: string;
        gender: number;
        id: number;
        name: string;
        order:number;
        profile_path?: string;
    }

    export interface Crew{
        credit_id: string;
        department: string;
        gender?: number;
        id: number;
        job: string;
        name: string;
        profile_path?: string;
    }
}