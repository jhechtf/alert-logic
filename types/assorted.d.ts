export interface Loadable{
    has_errors: boolean;
    loading: boolean;
}