import Vue from 'vue';
import Router, { RouteConfig } from 'vue-router';
import Home from './views/Home.vue';

export default new Router({
  mode: 'hash',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
    },
    {
      path: '/movie/:id',
      name: 'movie-details',
      component: () => import(/* webpackChunkName: "moviedetails" */ './views/MovieDetails.vue')
    },
    {
      path: '/search/:query/:page?',
      name: 'search-results',
      component: () => import(/* webpackChunkName: "searchresults" */ './views/SearchResults.vue')
    },
    {
      path: '/tv/:id',
      name: 'tv-details',
      component: () => import(/* webpackChunkName: "tvdetails" */ './views/TvDetails.vue')
    },
    {
      path: '/person/:id',
      name: 'person-details',
      component: () => import(/* webpackChunkName: "persondetails" */ './views/PersonDetails.vue')
    },
    {
      path: '*',
      component: () => import(/* webpackChunkName: "notfound" */ './views/NotFound.vue')
    }
  ],
});
