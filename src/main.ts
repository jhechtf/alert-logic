import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './App.vue';
import router from './router';
import store from './store';
import TmdbPlugin from '@lib/tmdb';
import config from '@/config/tmdb';
import BootstrapVue from 'bootstrap-vue';
import Loading from '@components/Loading.vue';
import Vuex from 'vuex';

Vue.config.productionTip = false;

// Use the TmdbPlugin
Vue.use(TmdbPlugin, {
  apiKey: config.apiKey
});
Vue.use(Vuex);
Vue.use(VueRouter);
// Use the Bootstrap Vue stuff
Vue.use(BootstrapVue);
// Figured it's just easier to default define the loading element so that anyone can use it.
Vue.component('Loading', Loading);

// Create the main Vue instance
new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
