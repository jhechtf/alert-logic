// TODO: see if we can turn this into a singleton instance? Unsure how that would work in TypeScript

// Export the Cookie class. this is a static class -- no need or point in calling "new Cookie"
class Cookie {
    // private _map, which has string keys to string values.
    /* tslint:disable-next-line:variable-name */
    private _map: Map<string, string> =  new Map();
    // When the cookie instance is created, parse the document.cookie string
    constructor() {
        this.parse();
    }
    /**
     * @param name The name of the cookie we are checking for
     * @returns the value of the cookie or undefined
     */
    public get(name: string): string | undefined {
        return this._map.get(name);
    }
    /**
     * @param name the name of the cookie
     * @param value the value of the cookie
     * @param expires a string or date object representing when the cookie will expire
     * @param path the path the cookie should be made available on.
     */
    public set(name: string, value: string, expires: string | Date = '', path?: string) {
        // Encode both ends because I'm paranoid
        let cookieString = [name, value].map((item) => encodeURIComponent(item)).join('=');
        // Modifiers is going to be an array of strings, so lets initialize it to an empty array
        const modifiers: string[] = [];
        // Run if expires is present (spoiler, it should be always present and equal to an empty string)
        if(expires) {
            modifiers.push('expires='+ ( typeof expires === 'string' ? expires : expires.toUTCString() ) );
        }
        // If path is present
        if(path) {
            modifiers.push('path='+path);
        }
        // If we have any modifiers, add them onto the cookie string
        if(modifiers.length) {
            cookieString+=';'+modifiers.join(';');
        }
        // Set everything onto the map set
        this._map.set(name, value);
        // actually set the cookie now
        document.cookie = cookieString;
    }
    /**
     * @param name the name of the cookie we are going to delete
     */
    public delete(name: string): void {
        // Today is the day
        const today: Date = new Date();
        // kidding! it's tomorrow
        today.setDate(today.getDate()-1);
        // Kidding, we're just going to remove this now.
        document.cookie = name+'=deleteMe;expires='+today.toUTCString();
        // delete the key from the map.
        this._map.delete(name);
    }
    /**
     * Parse the document.cookie string into a Map object
     */
    protected parse(): void {
        document.cookie.split(/;\s?/).forEach((cookie: string) => {
            /* tslint:disable-next-line:one-variable-per-declaration */
            let name: string, value: string;
            [name, value] = cookie.split('=').map((item) => decodeURIComponent(item));
            this._map.set(name, value);
        });
    }
}

export default (new Cookie());
