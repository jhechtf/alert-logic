import Vue, { VueConstructor } from 'vue';
import TmdbClass from './tmdb';

// TMDB interface options
interface tmdbOptions {
    apiKey: string;
    v4Key?: string;
}

// TmdbPlugin the plugin object to add the $tmdb instance type
// tslint:disable-next-line:variable-name
const TmdbPlugin = {
    /* tslint:disable-next-line:no-shadowed-variable variable-name */
    install(Vue: VueConstructor, options: tmdbOptions) {
        Vue.prototype.$tmdb = new TmdbClass( options.apiKey );
        // install the TMDB instance
    }
};

// Export TmdbPlugin
export default TmdbPlugin;
