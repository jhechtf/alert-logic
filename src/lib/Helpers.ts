// tslint:disable: variable-name
const CurrencyFormat = (amt: number): string => {
    const amtParts: string[] = amt.toString().split('');
    const retArr: string[] = []    ;
    while(amtParts.length) {
        retArr.unshift(amtParts.splice(-3).join('') );
    }
    return '$' + retArr.join(',');
};

const MinutesToHours = (minutes: number): string => {
    const hours = Math.floor(minutes / 60);
    const mins = minutes % 60;
    return `${hours}hr${mins}min`;
};

export {
    CurrencyFormat,
    MinutesToHours,
};
