export interface Loadable {
    loading: boolean;
    has_errors: boolean;
}
