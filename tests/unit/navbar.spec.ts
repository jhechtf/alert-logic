import {shallowMount, createLocalVue} from '@vue/test-utils';
import VueRouter from 'vue-router';
import Vuex from 'vuex';
import router from '@/router';
import store from '@/store';
import Navbar from '@components/Navbar.vue';

const localVue = createLocalVue();
localVue.use(VueRouter);
localVue.use(Vuex);
describe('Navbar.vue', ()=> {
    it('Should have a default state', ()=> {
        expect(store.state.searchQuery).toBe('');
    });
    it('Should mount', ()=> {
        const wrapper = shallowMount(Navbar,{
            localVue,
            store,
            router,
            stubs: ['router-link']
        });
        expect(wrapper.text()).toContain('Home');
    });
    it('Should have autocomplete results when a searchquery is given', ()=> {
        const wrapper = shallowMount(Navbar,{
            localVue,
            store,
            router,
            stubs: ['router-link']
        });
        expect(wrapper.vm.$data.searchQuery).toBe('');
        wrapper.vm.$store.commit('updateSearch', {searchQuery: 'harry potter'});
        expect(wrapper.vm.$store.state.searchQuery).toBe('harry potter');
    });
});
