const path = require('path');

const fromSrc = (...p) => path.join(__dirname, 'src', ...p);

module.exports = {
    chainWebpack: config => {
        config.resolve.alias
            .set('@lib', fromSrc('lib'));
        config.resolve.alias
            .set('@components', fromSrc('components'));
        config.resolve.alias
            .set('@assets', fromSrc('assets'));
    }
}